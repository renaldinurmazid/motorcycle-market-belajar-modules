@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header justify-content-between">
        <h1>Role Update</h1>
        <a href="{{route('role.index')}}" class="btn btn-primary">Back <i class="fas fa-arrow-left"></i></a>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <div class="form-group">
                <label for="">Role Name</label>
                <input type="text" class="form-control" value="{{$role->name}}" name="role_name" readonly>
            </div>
            <form action="{{route('role.give_permission', $role->id)}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="">Permission</label>
                    <div class="row">
                        @foreach ($permissions as $permission)
                        <div class="col-md-2">
                            <label>
                                <input type="checkbox" name="permission[]" value="{{ $permission->id }}"
                                    {{ in_array($permission->id, $rolePermissions) ? 'checked':'' }} />
                                {{ $permission->name }}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Submit <i class="fas fa-paper-plane"></i></button>
            </form>
        </div>
    </div>
</section>
@endsection
