@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Role</h1>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <h4>Role Table</h4>
                    @can('create role')
                    <a href="{{route('role.create')}}" class="btn btn-success">Add Role <i class="fas fa-plus"></i></a>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="table_data">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Role Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#table_data').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('role.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: '#'},
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        })
    })
</script>
@endsection
