<?php

namespace Modules\Role\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Role::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '';
                    if(auth()->user()->can('give permission')){
                        $btn = '<a href="'.route('role.add_permission', $row->id).'" class="edit btn btn-primary btn-sm">Tambahkan Permission <i class="fa fa-lock"></i></a>';
                    }
                    $btn .= '&nbsp;&nbsp;';

                    if(auth()->user()->can('edit role')){
                    $btn .= '<a href="'.route('role.edit', $row->id).'" class="edit btn btn-warning btn-sm">Edit <i class="fa fa-edit"></i></a>';
                    }
                    $btn .= '&nbsp;&nbsp;';

                    if(auth()->user()->can('delete role')){
                    $btn .= ' <a href="'.route('role.destroy', $row->id).'" class="delete btn btn-danger btn-sm">Delete <i class="fa fa-trash"></i></a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('role::index');
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('role::create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'role_name' => 'required','string','unique:roles,name',
        ]);

        Role::create([
            'name' => $request->input('role_name'),
        ]);

        return redirect()->route('role.index')->with('success', 'Role created successfully.');
    }

    /**
     * Show the specified resource.
     */
    public function show($id)
    {
        return view('role::show');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $role = Role::find($id);
        return view('role::edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'role_name' => 'required','string','unique:roles,name,',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('role_name');
        $role->save();

        return redirect()->route('role.index')->with('success', 'Role updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->route('role.index')->with('success', 'Role deleted successfully.');
    }

    public function add_permission($id) 
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        $rolePermissions = DB::table('role_has_permissions')
                                ->where('role_has_permissions.role_id', $role->id)
                                ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
                                ->all();
        return view('role::add_permission', compact('role', 'permissions', 'rolePermissions'));
    }

    public function give_permission(Request $request, $id)
    {
        $request->validate([
            'permission' => 'required|array',
        ]);

        $role = Role::find($id);

        $rolePermissions = $role->permissions->pluck('id')->toArray();

        $newPermissions = $request->input('permission');

        $permissionsToAdd = array_diff($newPermissions, $rolePermissions);
        if (!empty($permissionsToAdd)) {
            $permissions = Permission::whereIn('id', $permissionsToAdd)->get();
            $role->givePermissionTo($permissions);
        }

        $permissionsToRemove = array_diff($rolePermissions, $newPermissions);
        if (!empty($permissionsToRemove)) {
            $permissions = Permission::whereIn('id', $permissionsToRemove)->get();
            $role->revokePermissionTo($permissions);
        }

        return redirect()->route('role.index')->with('success', 'Permission updated successfully.');
    }


}
