<?php

use Illuminate\Support\Facades\Route;
use Modules\Role\Http\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function () {
    Route::resource('role', RoleController::class)->names('role');
    Route::get('role/{id}/destroy', [RoleController::class, 'destroy'])->name('role.destroy');
    Route::get('role/{id}/give_permission', [RoleController::class, 'add_permission'])->name('role.add_permission');
    Route::post('role/{id}/give_permission', [RoleController::class, 'give_permission'])->name('role.give_permission');
});
