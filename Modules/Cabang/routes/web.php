<?php

use Illuminate\Support\Facades\Route;
use Modules\Cabang\Http\Controllers\CabangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function () {
    Route::resource('cabang', CabangController::class)->names('cabang');
    Route::get('cabang/{id}/destroy', [CabangController::class, 'destroy'])->name('cabang.destroy');
    Route::get('cabang/{id}/detail_outlet', [CabangController::class, 'detail_outlet'])->name('cabang.detail_outlet');
});
