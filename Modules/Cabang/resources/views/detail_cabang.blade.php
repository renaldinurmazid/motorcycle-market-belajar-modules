@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Outlet - {{$cabang->outlet_name}}</h1>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <h4>Outlet Detail</h4>
                    <a href="{{route('cabang.index')}}" class="btn btn-primary">Laporan Penjualan PDF  <i class="fas fa-download"></i></a>
                </div>
                <div class="card-body">
                    <div class="mb-2">
                        <p class="m-0">Admin Name : {{$cabang->admin->name}}</p>
                        <p class="m-0">Address : {{$cabang->address}}</p>
                        <a href="" class="btn btn-primary">Stock Barang</a>
                    </div>
                    <div class="d-flex justify-content-between mb-2">
                        <input type="text" class="form-control" name="search" onchange="filteringdata(this)" id="search" placeholder="Search...">
                        <span class="btn btn-primary input-group-text" onclick="window.location='{{route('cabang.index')}}'"><i class="fas fa-times text-white"></i></span>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product Terjual</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>
@endsection
