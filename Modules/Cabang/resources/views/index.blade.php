@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Outlet</h1>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <h4>Outlet Table</h4>
                    @can('create cabang')
                    <a href="{{route('cabang.create')}}" class="btn btn-success">Add Outlet <i
                            class="fas fa-plus"></i></a>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="cabang-table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Outlet Name</th>
                                <th scope="col">Admin Name</th>
                                <th scope="col">Address</th>
                                {{-- <th scope="col">Income/Month</th> --}}
                                {{-- <th scope="col">Count Product</th> --}}
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>
<script>
    $(document).ready(function() {
        $('#cabang-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('cabang.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'outlet_name', name: 'outlet_name'},
                {data: 'admin', name: 'admin'},
                {data: 'address', name: 'address'},
                // {data: 'income', name: 'income'},
                // {data: 'count_product', name: 'count_product'},
                {data: 'action', name: 'action'},
            ]
        })
    })
</script>
@endsection
