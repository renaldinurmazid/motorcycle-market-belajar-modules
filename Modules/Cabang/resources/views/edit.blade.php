@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header justify-content-between">
        <h1>Outlet Create</h1>
        <a href="{{route('cabang.index')}}" class="btn btn-primary">Back <i class="fas fa-arrow-left"></i></a>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <form action="{{route('cabang.update', $cabang->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="">Outlet Name</label>
                    <input type="text" class="form-control" name="outlet_name" value="{{$cabang->outlet_name}}">
                </div>
                <div class="form-group">
                    <label for="">Admin Name</label>
                    <select name="admin_id" id="" class="form-control">
                        <option value="">Select Admin</option>
                        @foreach ($user as $item)
                        <option value="{{$item->id}}" {{ $cabang->admin_id == $item->id ? 'selected':'' }}>{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Address Outlet</label>
                    <textarea name="address" id="" cols="30" rows="10" class="form-control" >{{$cabang->address}}</textarea></textarea>
                </div>
                <button class="btn btn-primary" type="submit">Submit <i class="fas fa-paper-plane"></i></button>
            </form>
        </div>
    </div>
</section>
@endSection