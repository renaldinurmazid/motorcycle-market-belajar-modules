<?php

namespace Modules\Cabang\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cabang\Models\CabangM;

class CabangController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = CabangM::latest()->get();
            return datatables()->of($data)
                ->addIndexColumn()
                ->addColumn('admin', function($data){
                    $admin = $data->admin->name;
                    return $admin;
                })
                ->addColumn('action', function($data){
                    $btn = '';
                    $btn = '<a href="'.route('cabang.detail_outlet', $data->id).'" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm">Detail</a>';
                    $btn .= '&nbsp;&nbsp;';
                    if(auth()->user()->can('edit cabang')){
                        $btn .= '<a href="'.route('cabang.edit', $data->id).'" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-warning btn-sm editCabang">Edit</a>';
                    }
                    $btn .= '&nbsp;&nbsp;';
                    if(auth()->user()->can('delete cabang')){
                        $btn = $btn.' <a href="'.route('cabang.destroy', $data->id).'" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCabang">Delete</a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('cabang::index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user = User::whereHas('roles',function($q){
            $q->where('name','admin');
        })->get();
        return view('cabang::create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'outlet_name' => 'required',
            'admin_id' => 'required','exists:users,id','unique:cabang,admin_id',
            'address' => 'required',
        ]);

        CabangM::create([
            'outlet_name' => $request->outlet_name,
            'admin_id' => $request->admin_id,
            'address' => $request->address,
        ]);

        return redirect()->route('cabang.index')->with('success', 'Cabang created successfully.');
    }

    /**
     * Show the specified resource.
     */
    public function show($id)
    {
        return view('cabang::show');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $cabang = CabangM::find($id);
        $user = User::whereHas('roles',function($q){
            $q->where('name','admin');
        })->get();
        return view('cabang::edit', compact('cabang', 'user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'outlet_name' => 'required',
            'admin_id' => 'required','exists:users,id','unique:cabang,admin_id',
            'address' => 'required',
        ]);

        CabangM::find($id)->update([
            'outlet_name' => $request->outlet_name,
            'admin_id' => $request->admin_id,
            'address' => $request->address,
        ]);

        return redirect()->route('cabang.index')->with('success', 'Cabang updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $cabang = CabangM::find($id);
        $cabang->delete();
        return redirect()->route('cabang.index')->with('success', 'Cabang deleted successfully.');
    }

    public function detail_outlet($id)
    {
        $cabang = CabangM::find($id);
        return view('cabang::detail_cabang', compact('cabang'));
    }
}
