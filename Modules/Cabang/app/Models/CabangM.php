<?php

namespace Modules\Cabang\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Cabang\Database\factories\CabangMFactory;

class CabangM extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'id','admin_id','outlet_name','address','created_at','updated_at'
    ];
    protected $table = 'cabang';
    
    public function admin()
    {
        return $this->belongsTo(User::class);
    }
}
