@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header justify-content-between">
        <h1>Permission Create</h1>
        <a href="{{route('permission.index')}}" class="btn btn-primary">Back <i class="fas fa-arrow-left"></i></a>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <form action="{{route('permission.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="">Permission Name</label>
                    <input type="text" class="form-control" name="permission_name">
                </div>
                <button class="btn btn-primary" type="submit">Submit <i class="fas fa-paper-plane"></i></button>
            </form>
        </div>
    </div>
</section>
@endSection