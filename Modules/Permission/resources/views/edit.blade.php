@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header justify-content-between">
        <h1>Permission Update</h1>
        <a href="{{route('permission.index')}}" class="btn btn-primary">Back <i class="fas fa-arrow-left"></i></a>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <form action="{{route('permission.update', $permission->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="">Permission Name</label>
                    <input type="text" class="form-control" value="{{$permission->name}}" name="permission_name">
                </div>
                <button class="btn btn-primary" type="submit">Submit <i class="fas fa-paper-plane"></i></button>
            </form>
        </div>
    </div>
</section>
@endSection