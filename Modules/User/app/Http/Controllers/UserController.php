<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data =  User::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('role', function($row){
                    $role = $row->getRoleNames()->first();
                    return $role;
                })
                ->addColumn('action', function($row){
                    $btn = '';
                    if(auth()->user()->can('edit user')){
                        $btn = '<a href="'.route('user.edit', $row->id).'" class="edit btn btn-primary btn-sm">Edit</a>';
                    }
                    $btn .= '&nbsp;&nbsp;';

                    if(auth()->user()->can('delete user')){
                        $btn = $btn.' <a href="'.route('user.destroy', $row->id).'" class="btn btn-danger btn-sm">Delete</a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('user::index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $role = Role::all();
        return view('user::create', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required','unique:users,email',
            'role' => 'required',
            'password' => 'required',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $user->assignRole($request->role);

        return redirect()->route('user.index')->with('success', 'User created successfully.');
    }

    /**
     * Show the specified resource.
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $role = Role::all();
        $user = User::find($id);
        return view('user::edit', compact('user', 'role'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'role' => 'required','exists:roles,name',
        ]);

        $user = User::find($id);

        $user->removeRole($user->getRoleNames()->first());

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        $user->assignRole($request->role);

        return redirect()->route('user.index')->with('success', 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('user.index')->with('success', 'User deleted successfully.');
    }
}
