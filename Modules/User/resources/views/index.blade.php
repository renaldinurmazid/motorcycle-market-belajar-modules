@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>User</h1>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <h4>User Table</h4>
                    @can('create user')
                    <a href="{{route('user.create')}}" class="btn btn-success">Add User <i class="fas fa-plus"></i></a>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="user-table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Gmail</th>
                                <th scope="col">Role</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>
<script>
    $(document).ready(function() {
        $('#user-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{route('user.index')}}",
            responsive: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'role', name: 'role'},
                {data: 'action', name: 'action'},
            ]
        })
    })
</script>
@endsection
