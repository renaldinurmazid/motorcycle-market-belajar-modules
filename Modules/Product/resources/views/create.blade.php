@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header justify-content-between">
        <h1>Product Create</h1>
        <a href="{{route('product.index')}}" class="btn btn-primary">Back <i class="fas fa-arrow-left"></i></a>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <form action="{{route('product.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="">Product Name</label>
                    <input type="text" class="form-control" name="product_name">
                </div>
                <div class="form-group">
                    <label for="">Product Price</label>
                    <input type="number" class="form-control" name="product_price">
                </div>
                <div class="form-group">
                    <label for="">Qty</label>
                    <input type="number" class="form-control" name="qty">
                </div>
                <div class="form-group">
                    <label for="">Cabang</label>
                    <select name="cabang_id" id="" class="form-control">
                        <option value="">Select Cabang</option>
                        @foreach ($cabang as $item)
                            <option value="{{$item->id}}">{{$item->outlet_name}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-primary" type="submit">Submit <i class="fas fa-paper-plane"></i></button>
            </form>
        </div>
    </div>
</section>
@endSection