@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header d-flex justify-content-between">
        <h1>Product</h1>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <h4>Product Table</h4>
                    <div class="d-flex align-items-center">
                        @can('create product')
                        <a href="{{route('product.create')}}" class="btn btn-success">Add Product <i class="fas fa-plus"></i></a>
                        @endcan
                        @can('filter product by outlet')
                        <select name="cabang_id" id="outlet-filter" class="form-control" style="height: 30px;font-size: 12px;width: 200px;padding: 0 10px">
                            <option value="">Select Cabang</option>
                            @foreach ($cabang as $item)
                                <option value="{{$item->id}}">{{$item->outlet_name}}</option>
                            @endforeach
                        </select>
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="product-table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Product Price</th>
                                <th scope="col">Outlet Name</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>

<script>
    $(document).ready(function() {
    $('#product-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('product.index') }}",
            data: function (d) {
                d.cabang_id = $('#outlet-filter').val();
            }
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'product_name', name: 'product_name' },
            { data: 'product_price', name: 'product_price' , render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' )},
            { data: 'cabang.outlet_name', name: 'cabang.outlet_name' },
            { data: 'qty', name: 'qty' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ]
    });

    $('#outlet-filter').on('change', function () {
        $('#product-table').DataTable().ajax.reload();
    });
});

</script>
@endsection
