<?php

namespace Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Cabang\Models\CabangM;
use Modules\Product\Database\factories\ProductMFactory;

class ProductM extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'id','product_name','product_price','qty','cabang_id'
    ];
    protected $table = 'product';

    public function cabang()
    {
        return $this->belongsTo(CabangM::class);
    }
    
}
