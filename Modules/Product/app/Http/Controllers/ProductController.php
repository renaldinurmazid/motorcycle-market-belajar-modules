<?php

namespace Modules\Product\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Cabang\Models\CabangM;
use Modules\Product\Models\ProductM;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $userRole = auth()->user()->roles->pluck('name');
            $cabang_id = $request->input('cabang_id');
            $productsQuery = ProductM::query()->with('cabang');

            if($userRole->contains('Superadmin')){
                if($cabang_id){
                    $productsQuery->where('cabang_id', $cabang_id);
                }
            } else {
                $userBranch = Auth::user()->cabang->id;
                $productsQuery->where('cabang_id', $userBranch);
            }

            $products = $productsQuery->get();

            return DataTables::of($products)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn ='';
                    if(auth()->user()->can('edit product')){
                        $btn = '<a href="'.route('product.edit', $row->id).'" class="edit btn btn-warning btn-sm">Edit <i class="fa fa-edit"></i></a>';
                    }
                    $btn .= '&nbsp;&nbsp;';

                    if(auth()->user()->can('delete product')){
                        $btn .= '<a href="'.route('product.destroy', $row->id).'" class="edit btn btn-danger btn-sm">Delete <i class="fa fa-trash"></i></a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        $cabang = CabangM::all();
        return view('product::index', compact('cabang'));
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $cabang = CabangM::all();
        return view('product::create', compact('cabang'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'product_name' => 'required',
            'product_price' => 'required',
            'cabang_id' => 'required','exists:cabang,id',
            'qty' => 'required',
        ]);

        ProductM::create([
            'product_name' => $request->product_name,
            'product_price' => $request->product_price,
            'cabang_id' => $request->cabang_id,
            'qty' => $request->qty
        ]);

        return redirect()->route('product.index')->with('success', 'Product created successfully.');
    }

    /**
     * Show the specified resource.
     */
    public function show($id)
    {
        return view('product::show');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $product = ProductM::find($id);
        $cabang = CabangM::all();
        return view('product::edit', compact('product', 'cabang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'product_name' => 'required',
            'product_price' => 'required',
            'cabang_id' => 'required','exists:cabang,id',
            'qty' => 'required',
        ]);

        ProductM::find($id)->update([
            'product_name' => $request->product_name,
            'product_price' => $request->product_price,
            'cabang_id' => $request->cabang_id,
            'qty' => $request->qty
        ]);

        return redirect()->route('product.index')->with('success', 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        ProductM::find($id)->delete();
        return redirect()->route('product.index')->with('success', 'Product deleted successfully.');
    }
}
